Welcome to The Ferris Framework
===============================

Ferris is a rapid application framework created specifically for Google App Engine. This framework is designed to get you up and running as quickly as possible with flexible, granular scaffolding for common CRUD operations. Ferris is inspired by Ruby on Rails, CakePHP, Django, and Flask.

Ferris is licensed under the `Apache License, Version 2.0 <http://www.apache.org/licenses/LICENSE-2.0.html>`_.

Features
--------
* Uses native App Engine libraries for data modeling.
* Familiar model-view-whatever style architecture.
* Automatic routing with the ability to specify completely custom routes.
* Granular and flexible handler and template scaffolding.
* Powerful template and theme engine built on `Jinja2 <http://jinja.pocoo.org/>`_.
* Integration with `wtforms <http://wtforms.simplecodes.com/>`_.
* Global (application-wide) and local (within object) events system.
* Plugin architecture.
* OAuth2 consumer support built-in.
* Pre-packaged Google API Client, GData Client, and PyTZ for App Engine.


A Quick Look
------------

A simple blog application could be written like this::

    from ferris import Model, Controller, scaffold

    class Post(Model):
        title = ndb.StringProperty()
        content = ndb.TextProperty()

    class Posts(Controller):
        class Meta:
            prefixes = ('admin',)
            components = (scaffold.Scaffolding,)
            Model = Post

        list = scaffold.list
        view = scaffold.view
        admin_list = scaffold.list
        admin_view = scaffold.view
        admin_add = scaffold.add
        admin_edit = scaffold.edit
        admin_delete = scaffold.delete

At this point, a full administrative interface for creating Posts would be available at ``/admin/posts`` and a publicly accessable list and view would be present at ``/posts``.


Documentation
=============

.. toctree::
  :includehidden:
  :maxdepth: 2

  new_in_two
  getting_started
  tutorial/1_introduction
  users_guide/index


Indexes
=======

If you can't find the information you're looking for, have a look at the
index or try to find it using the search function:

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`



