What's New In Version 2
=======================

Changelog:

    * Major overhaul of the controller part of Ferris' MVC. We've wholesale replaced handlers with the new :doc:`users_guide/controllers`. 
    * Third-party packages have been updated to newer versions.
    * New admin layout.
    * Bootstrap updated from 2 to 3.
    * :doc:`users_guide/scaffolding`  has been completely re-done. It's now a component and doesn't involve magic to make it work.
    * New scaffold views.
    * EasyHandler has been removed. With the new scaffold it's very easily to replicate its functionality.
    * Template tester plugin as been removed.
    * Scaffold only supports urlsafe keys, no more direct support for IDs.
    * New Cache component.
    * Memcache utilties have been improved for usability.
    * Protorpc :doc:`users_guide/messages` integration for REST APIs.
    * :doc:`users_guide/search` API integration.
    * Value formatters for pretty-printing in templates.
    * Request parsers for a unified API for processing Forms and Messages.
    * Response handlers to transform return values into responses.
    * View classes to help generate complex responses.
    * Test runner has been broken out into Ferrisnose.
    * Pagination overhauled with better support for tracking cursors.
    * TinyMCE has been removed (a seperate repository called ferris-fancy-forms is available).
    * Template events
    * Authorization chains
    * Removing our custom wtforms.appengine.ext.ndb module in favor of the built-in wtforms one.
    * Mail component removed, use ferris.core.mail instead.
    * app.yaml cleanup, Ferris is now included into app.yaml.
    * Custom app for handling deferred tasks, prevents import errors.
    * Plugin to manage OAuth2 service accounts.
    * JSON component removed in favor of Messages.
    * Higher test coverage.
    * Fixing various memory leaks.
    * Various bug fixes.
